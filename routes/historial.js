const { Router } = require('express');
const { buscarHistorial } = require('../controllers/historialController');

const router = Router();

router.get('/get', buscarHistorial )

module.exports = router;
