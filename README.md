# RestServer

Backend  Servicio GET Array:

Servicio GET localhost:4500/api/historial/get

Response:

{
"results": [
{
"id": 1,
"first_name": "Trever",
"last_name": "Gowan",
"email": "tgowan0@blinklist.com",
"gender": "Non-binary",
"ip_address": "54.156.144.136"
},
{
"id": 2,
"first_name": "Aubrie",
"last_name": "Nayer",
"email": "anayer1@tinyurl.com",
"gender": "Bigender",
"ip_address": "167.76.93.62"
},
{
"id": 3,
"first_name": "Darren",
"last_name": "Rubenov",
"email": "drubenov2@stumbleupon.com",
"gender": "Female",
"ip_address": "179.3.121.124"
},
{
"id": 4,
"first_name": "Bruis",
"last_name": "Destouche",
"email": "bdestouche3@acquirethisname.com",
"gender": "Agender",
"ip_address": "186.67.18.172"
},
{
"id": 5,
"first_name": "Audre",
"last_name": "Dacey",
"email": "adacey4@zimbio.com",
"gender": "Agender",
"ip_address": "3.203.162.4"
}
]
}
