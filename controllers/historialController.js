const { response } = require('express');
const fs = require ('fs');


const buscarHistorial = ( req, res = response ) => {

    historial = [];
    dbPath = './bd/historial.json';

    if( !fs.existsSync( dbPath ) ) return;

    const info = fs.readFileSync( dbPath, { encoding: 'utf-8' });
    const data = JSON.parse( info );

    this.historial = data.historial;

    res.json({
        results: this.historial
    });
}

    module.exports = {
        buscarHistorial
}
